<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
   <title>在线考试测评系统登陆界面</title>
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
	<link href="<%=basePath%>css/style.css" rel="stylesheet" type="text/css" />
	<script language="JavaScript" src="js/jquery.js"></script>
	<script src="<%=basePath%>js/cloud.js" type="text/javascript"></script>
	<script type="text/javascript" src="<%=basePath%>js/login/login.js"></script>
	<%-- <script type="text/javascript" src="<%=basePath%>js/xmlHttpRequest.js"></script> --%>
	
	<script language="javascript">
		$(function(){
	    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
		$(window).resize(function(){  
	    $('.loginbox').css({'position':'absolute','left':($(window).width()-692)/2});
	    })  
	});  
</script> 

   </head>
  
 <body style="background-color:#1c77ac; background-image:url(images/light.png); background-repeat:no-repeat; background-position:center top; overflow:hidden;">

    <div id="mainBody">
      <div id="cloud1" class="cloud"></div>
      <div id="cloud2" class="cloud"></div>
    </div>

	<div class="logintop">
		<span>欢迎登录在线考试测评系统</span>
	</div>
	<div class="loginbody">
	    <span class="systemlogo"></span> 
	    <div class="loginbox">
		<div class="msg">
			<span id="msg"><font class="msgFont">${msg }</font>
			</span>
		</div>

		<div class="formStyle">
			<form action="<%=request.getContextPath()%>/userAction_login" method="post"
				onsubmit="return checkLogin()">
				<table border="3px solid black">
					<tr>
						<td><input type="text" name="username" id="userId"
							class="loginuser" placeholder="请输入用户名" onblur="checkUserId()" /></td>
						<td><label id="checkUserId"></label></td>
					</tr>
					<tr>
						<td><input type="password" name="pwd" id="userPwd"
							class="loginPwd" placeholder="请输入登录密码" onblur="checkUserPwd()" /></td>
						<td><label id="checkUserPwd"></label></td>
					</tr>
					<tr>
						<td><input type="text" name="imgCode" id="imgCode"
							class="imgCode" placeholder="请输入验证码" onblur="checkUserImgCode()" />
							<img src="<%=basePath%>imageCodeAction_getImageCode"
							class="imageCode" name="imgFigure" id="imgFigure" onClick="changeImage()" /></td>
						<td><label id="checkUserImgCode"></label></td>
					</tr>
					<tr>
						<td><input type="radio" value="1" name="role"
							<c:if test="${role ==null }" >checked</c:if>
							<c:if test="${role eq '1' }" >checked</c:if> />&nbsp;<font
							class="fontStyle">系统管理员</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
							type="radio" value="2" name="role"
							<c:if test="${role eq '2' }" >checked</c:if> />&nbsp;<font
							class="fontStyle">教师</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input
							type="radio" value="3" name="role"
							<c:if test="${role eq '3' }" >checked</c:if> />&nbsp;<font
							class="fontStyle">学生</font>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
						<td><label id="checkUserType"></label></td>
					</tr>
					<tr>
						<td colspan="2"><input type="submit" class="loginbtn"
							value="登录" /> <input type="button" class="loginbtn" value="重置"
							style="margin-left:30px;" onClick="resetUser()" /></td>
					</tr>
					<tr>
						<td colspan="2">
							<label><input name="" type="checkbox" value="" checked="checked" style="margin-left:10px" />记住密码</label>
						</td>
						<td>
				</table>
			</form>
		</div>
	</div>
    </div>
</body>
</html>
