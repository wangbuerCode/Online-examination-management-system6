package com.onlineExam.action;

import java.util.List;

import com.onlineExam.entity.Subject;
import com.onlineExam.entity.Testanswer;
import com.onlineExam.entity.Testquestions;
import com.onlineExam.service.SubjectService;
import com.onlineExam.service.TestanswerService;
import com.onlineExam.service.TestquestionsService;
import com.opensymphony.xwork2.ActionContext;

public class TestanswerAction {

	private TestanswerService biz;
	private TestquestionsService testquestionsService;
	private SubjectService subjectService;

	public TestanswerService getBiz() {
		return biz;
	}

	public void setBiz(TestanswerService biz) {
		this.biz = biz;
	}

	public TestquestionsService getTestquestionsService() {
		return testquestionsService;
	}

	public void setTestquestionsService(TestquestionsService testquestionsService) {
		this.testquestionsService = testquestionsService;
	}

	public SubjectService getSubjectService() {
		return subjectService;
	}

	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	public String findTestanswerAll() {
		Testquestions testquestions = testquestionsService.findSingleTestquestions(testquestionsid);
		ActionContext.getContext().put("testquestions", testquestions);
		
		Subject subject = subjectService.findSingleSubject(testquestions.getSid());
		ActionContext.getContext().put("subjectname", subject.getSubjectName());
		
		List<Testanswer> testanswerList = biz.findTestanswerAll(testquestionsid);
		ActionContext.getContext().put("testanswerList", testanswerList);

		return "to_testanswer_list";
	}

	public String beforeAddOrUpdateTestanswer() {
		testanswerobj = new Testanswer();
		Testanswer testanswer = new Testanswer();
		if (testanswerid != null && !testanswerid.equals("")) {
			testanswer = biz.findSingleTestanswer(testanswerid);
		}
		ActionContext.getContext().put("testanswer", testanswer);
		return "to_addOrEditTestanswer";
	}

	public String addOrUpdateTestanswer() {
		biz.addOrUpdateTestanswer(testanswerobj);
		return "to_testanswer_listAction";
	}

	public String deleteTestanswer() {
		biz.deleteTestanswer(testanswerid);
		return "to_testanswer_listAction";
	}

	private Integer testanswerid;
	private Testanswer testanswerobj;
	private Integer testquestionsid;

	public Integer getTestanswerid() {
		return testanswerid;
	}

	public void setTestanswerid(Integer testanswerid) {
		this.testanswerid = testanswerid;
	}

	public Testanswer getTestanswerobj() {
		return testanswerobj;
	}

	public void setTestanswerobj(Testanswer testanswerobj) {
		this.testanswerobj = testanswerobj;
	}

	public Integer getTestquestionsid() {
		return testquestionsid;
	}

	public void setTestquestionsid(Integer testquestionsid) {
		this.testquestionsid = testquestionsid;
	}

}
