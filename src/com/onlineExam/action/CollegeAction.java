package com.onlineExam.action;

import java.util.List;

import com.onlineExam.entity.College;
import com.onlineExam.service.CollegeService;
import com.opensymphony.xwork2.ActionContext;

public class CollegeAction {

	private CollegeService biz;

	public CollegeService getBiz() {
		return biz;
	}

	public void setBiz(CollegeService biz) {
		this.biz = biz;
	}

	public String findCollegeAll() {
		List<College> collegeList = biz.findCollegeAll();
		ActionContext.getContext().put("collegeList", collegeList);
		return "to_college_list";
	}

	public String beforeAddOrUpdateCollege() {
		collegeobj = new College();
		College college = new College();
		if (collegeid != null && !collegeid.equals("")) {
			college = biz.findSingleCollege(collegeid);
		}
		ActionContext.getContext().put("college", college);
		return "to_addOrEditCollege";
	}

	public String addOrUpdateCollege() {
		biz.addOrUpdateCollege(collegeobj);
		return "to_college_listAction";
	}

	public String deleteCollege() {
		biz.deleteCollege(collegeid);
		return "to_college_listAction";
	}

	public String deleteColleges() {
		String[] ids = collegeids.split(",");
		if (ids.length > 0) {
			for (String id : ids) {
				biz.deleteCollege(Integer.parseInt(id.trim()));
			}
		}
		return "to_college_listAction";
	}

	private Integer collegeid;
	private String collegeids;
	private College collegeobj;

	public Integer getCollegeid() {
		return collegeid;
	}

	public void setCollegeid(Integer collegeid) {
		this.collegeid = collegeid;
	}

	public String getCollegeids() {
		return collegeids;
	}

	public void setCollegeids(String collegeids) {
		this.collegeids = collegeids;
	}

	public College getCollegeobj() {
		return collegeobj;
	}

	public void setCollegeobj(College collegeobj) {
		this.collegeobj = collegeobj;
	}

}
