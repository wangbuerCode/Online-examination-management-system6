package com.onlineExam.action;

import java.util.ArrayList;
import java.util.List;

import com.alibaba.fastjson.JSON;
import com.onlineExam.entity.Testanswer;
import com.onlineExam.entity.Testquestions;
import com.onlineExam.entity.Userexam;
import com.onlineExam.entity.Userexamanswer;
import com.onlineExam.service.TestanswerService;
import com.onlineExam.service.TestquestionsService;
import com.onlineExam.service.UserexamService;
import com.onlineExam.service.UserexamanswerService;
import com.opensymphony.xwork2.ActionContext;

public class UserexamanswerAction {

	private UserexamanswerService biz;
	private TestquestionsService testquestionsService;
	private TestanswerService testanswerService;
	private UserexamService userexamService;

	public UserexamanswerService getBiz() {
		return biz;
	}

	public void setBiz(UserexamanswerService biz) {
		this.biz = biz;
	}

	public TestquestionsService getTestquestionsService() {
		return testquestionsService;
	}

	public void setTestquestionsService(
			TestquestionsService testquestionsService) {
		this.testquestionsService = testquestionsService;
	}

	public TestanswerService getTestanswerService() {
		return testanswerService;
	}

	public void setTestanswerService(TestanswerService testanswerService) {
		this.testanswerService = testanswerService;
	}

	public UserexamService getUserexamService() {
		return userexamService;
	}

	public void setUserexamService(UserexamService userexamService) {
		this.userexamService = userexamService;
	}

	public String findUserexamanswerAll() {
		List<Userexamanswer> userexamanswerList = biz.findUserexamanswerAll();
		ActionContext.getContext()
				.put("userexamanswerList", userexamanswerList);
		return "to_userexamanswer_list";
	}

	public String beforeAddOrUpdateUserexamanswer() {
		userexamanswerobj = new Userexamanswer();
		Userexamanswer userexamanswer = new Userexamanswer();
		if (userexamanswerid != null && !userexamanswerid.equals("")) {
			userexamanswer = biz.findSingleUserexamanswer(userexamanswerid);
		}
		ActionContext.getContext().put("userexamanswer", userexamanswer);
		return "to_addOrEditUserexamanswer";
	}

	public String addOrUpdateUserexamanswer() {
		biz.addOrUpdateUserexamanswer(userexamanswerobj);
		return "to_userexamanswer_listAction";
	}

	public String deleteUserexamanswer() {
		biz.deleteUserexamanswer(userexamanswerid);
		return "to_userexamanswer_listAction";
	}

	public String jiaojuan() {
		System.out.println(JSON.toJSONString(answers1));
		System.out.println(JSON.toJSONString(answers2));
		System.out.println(JSON.toJSONString(answers3));
		System.out.println(JSON.toJSONString(answers4));
		
		System.out.println(userexamid);
		
		try {
			for (Userexamanswer a : answers1) {
				a.setUeid(userexamid);
				if (a.getQtype().contains("1") && a.getTaid() != null) {
					Testanswer aa = testanswerService.findSingleTestanswer(a.getTaid());
					Testquestions qq = testquestionsService.findSingleTestquestions(aa.getTqid());
					a.setIsRight(aa.getIsRight());
					a.setQtype(qq.getTesttype());
					if("Y".equals(a.getIsRight())){
						a.setScorce(qq.getScore());	
					}else{
						a.setScorce(0);
					}
					a.setTaname(aa.getAnswerInfo());
					a.setTqid(String.valueOf(qq.getId()));
					a.setTqname(qq.getTesttitle());
					biz.addOrUpdateUserexamanswer(a);
				}
			}
			
			for (Userexamanswer a : answers2) {
				a.setUeid(userexamid);
				if(a.getQtype().contains("2") && a.getTaname() != null&&!"".equals(a.getTaname())){
					String[] aid = a.getTaname().split(",");
					for (String id : aid) {
						Testanswer aa = testanswerService.findSingleTestanswer(Integer.parseInt(id.trim()));
						Testquestions qq = testquestionsService.findSingleTestquestions(aa.getTqid());
						a.setId(null);
						a.setIsRight(aa.getIsRight());
						a.setTaid(Integer.parseInt(id.trim()));
						a.setQtype(qq.getTesttype());
						if("Y".equals(a.getIsRight())){
							a.setScorce(qq.getScore());	
						}else{
							a.setScorce(0);
						}
						a.setTaname(aa.getAnswerInfo());
						a.setTqid(String.valueOf(qq.getId()));
						a.setTqname(qq.getTesttitle());
						biz.addOrUpdateUserexamanswer(a);
					}
				}
			}
			
			for (Userexamanswer a : answers3) {
				a.setUeid(userexamid);
				if(a.getQtype().contains("3") && a.getTaid() != null){
					Testanswer aa = testanswerService.findSingleTestanswer(a.getTaid());
					Testquestions qq = testquestionsService.findSingleTestquestions(aa.getTqid());
					a.setTaid(a.getTaid());
					a.setQtype(qq.getTesttype());
					if("Y".equals(a.getIsRight())){
						a.setScorce(qq.getScore());
					}else{
						a.setScorce(0);
					}
					a.setTaname(aa.getAnswerInfo());
					a.setTqid(String.valueOf(qq.getId()));
					a.setTqname(qq.getTesttitle());
					biz.addOrUpdateUserexamanswer(a);
				}
			}
			
			for (Userexamanswer a : answers4) {
				a.setUeid(userexamid);
				if(a.getQtype().contains("4") && a.getTaid() != null){
					Testanswer aa = testanswerService.findSingleTestanswer(a.getTaid());
					Testquestions qq = testquestionsService.findSingleTestquestions(aa.getTqid());
					a.setTaid(a.getTaid());
					a.setQtype(qq.getTesttype());
					a.setTaname(aa.getAnswerInfo());
					a.setTqid(String.valueOf(qq.getId()));
					a.setTqname(qq.getTesttitle());
					biz.addOrUpdateUserexamanswer(a);
				}
			}
			
			Userexam userexam = userexamService.findSingleUserexam(userexamid);
			userexam.setFinishflag("Y");
			userexamService.addOrUpdateUserexam(userexam);
			
			answers1=new ArrayList<Userexamanswer>();
			answers2=new ArrayList<Userexamanswer>();
			answers3=new ArrayList<Userexamanswer>();
			answers4=new ArrayList<Userexamanswer>();
			
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "to_userexam_listAction";
	}

	private Integer userexamanswerid;
	private Userexamanswer userexamanswerobj;

	private Integer userexamid;
	private List<Userexamanswer> answers1;
	private List<Userexamanswer> answers2;
	private List<Userexamanswer> answers3;
	private List<Userexamanswer> answers4;

	public Integer getUserexamanswerid() {
		return userexamanswerid;
	}

	public void setUserexamanswerid(Integer userexamanswerid) {
		this.userexamanswerid = userexamanswerid;
	}

	public Userexamanswer getUserexamanswerobj() {
		return userexamanswerobj;
	}

	public void setUserexamanswerobj(Userexamanswer userexamanswerobj) {
		this.userexamanswerobj = userexamanswerobj;
	}

	public Integer getUserexamid() {
		return userexamid;
	}

	public void setUserexamid(Integer userexamid) {
		this.userexamid = userexamid;
	}

	public List<Userexamanswer> getAnswers1() {
		return answers1;
	}

	public void setAnswers1(List<Userexamanswer> answers1) {
		this.answers1 = answers1;
	}

	public List<Userexamanswer> getAnswers2() {
		return answers2;
	}

	public void setAnswers2(List<Userexamanswer> answers2) {
		this.answers2 = answers2;
	}

	public List<Userexamanswer> getAnswers3() {
		return answers3;
	}

	public void setAnswers3(List<Userexamanswer> answers3) {
		this.answers3 = answers3;
	}

	public List<Userexamanswer> getAnswers4() {
		return answers4;
	}

	public void setAnswers4(List<Userexamanswer> answers4) {
		this.answers4 = answers4;
	}


}
