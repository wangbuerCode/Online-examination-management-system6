package com.onlineExam.action;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.onlineExam.entity.Exam;
import com.onlineExam.entity.Examquestions;
import com.onlineExam.entity.Testanswer;
import com.onlineExam.entity.User;
import com.onlineExam.entity.Userexam;
import com.onlineExam.entity.Userexamanswer;
import com.onlineExam.service.ExamService;
import com.onlineExam.service.TestanswerService;
import com.onlineExam.service.UserService;
import com.onlineExam.service.UserexamService;
import com.onlineExam.service.UserexamanswerService;
import com.opensymphony.xwork2.ActionContext;

public class UserexamAction {

	private UserexamService biz;
	private ExamService examService;
	private UserService userService;
	private TestanswerService testanswerService;
	private UserexamanswerService userexamanswerService;

	public UserexamService getBiz() {
		return biz;
	}

	public void setBiz(UserexamService biz) {
		this.biz = biz;
	}

	public ExamService getExamService() {
		return examService;
	}

	public void setExamService(ExamService examService) {
		this.examService = examService;
	}

	public UserService getUserService() {
		return userService;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}

	public TestanswerService getTestanswerService() {
		return testanswerService;
	}

	public void setTestanswerService(TestanswerService testanswerService) {
		this.testanswerService = testanswerService;
	}

	public UserexamanswerService getUserexamanswerService() {
		return userexamanswerService;
	}

	public void setUserexamanswerService(
			UserexamanswerService userexamanswerService) {
		this.userexamanswerService = userexamanswerService;
	}

	public String findUserexamAll() {
		Integer uid = (Integer) ActionContext.getContext().getSession()
				.get("uid");
		String roler = (String) ActionContext.getContext().getSession()
				.get("roler");
		if ("3".equals(roler)) {
			List<Userexam> userexamList = biz.findUserexamAllByUid(uid);
			ActionContext.getContext().put("userexamList", userexamList);
		} else {
			List<Userexam> userexamList = biz.findUserexamAll();
			ActionContext.getContext().put("userexamList", userexamList);
		}

		return "to_userexam_list";
	}
	
	public String findChar() {
		List<Object[]> list = biz.findUserexamAllTj();
		List<String> xAxis=new ArrayList<String>();
		List<Integer> datas=new ArrayList<Integer>();
		
		if(list!=null&&list.size()>0){
			for (Object[] obj : list) {
				xAxis.add(obj[0].toString());
				datas.add(Integer.parseInt(obj[1].toString()));
			}	
		}
		ActionContext.getContext().put("xAxis", xAxis);
		ActionContext.getContext().put("datas", datas);
		return "to_char";
	}

	public String beforeAddOrUpdateUserexam() {
		userexamobj = new Userexam();
		Userexam userexam = new Userexam();
		if (userexamid != null && !userexamid.equals("")) {
			userexam = biz.findSingleUserexam(userexamid);
		}
		ActionContext.getContext().put("userexam", userexam);
		return "to_addOrEditUserexam";
	}

	public String addOrUpdateUserexam() {
		biz.addOrUpdateUserexam(userexamobj);
		return "to_userexam_listAction";
	}

	public String deleteUserexam() {
		biz.deleteUserexam(userexamid);
		return "to_userexam_listAction";
	}

	private String pid;
	private String scores;

	public String getPid() {
		return pid;
	}

	public void setPid(String pid) {
		this.pid = pid;
	}

	public String getScores() {
		return scores;
	}

	public void setScores(String scores) {
		this.scores = scores;
	}

	public String yuejuan() {

		
		Integer uid = (Integer) ActionContext.getContext().getSession().get("uid");
		User user = userService.findSingleUser(uid);
		Userexam userexam = biz.findSingleUserexam(userexamid);
		int score = userexam.getScore();
		
		userexam.setCorrectingflag("Y");
		userexam.setCorrectinguid(uid);
		userexam.setCorrectinguname(user.getUsername());
		
		
		try {
			String[] pidArr = pid.split(",");
			String[] scoreArr = scores.split(",");
			
			if(pidArr.length==scoreArr.length){
				for (int i = 0; i < scoreArr.length; i++) {
					//获取学生自主题答案
					Userexamanswer aa = userexamanswerService.findUserexamanswerAllByInfo(userexamid,Integer.parseInt(pidArr[i].trim()));
					if(aa!=null){
						aa.setScorce(Integer.parseInt(scoreArr[i].trim()));
						userexamanswerService.addOrUpdateUserexamanswer(aa);
						score+=Integer.parseInt(scoreArr[i].trim());	
					}
					
				}
			}
			
			userexam.setScore(score);
			biz.addOrUpdateUserexam(userexam);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return "to_userexam_listAction";
	}

	// 教师查看试卷
	public String findUserExam() {
		int score = 0;
		Userexam userexam = biz.findSingleUserexam(userexamid);
		ActionContext.getContext().put("userexam", userexam);

		Exam exam = examService.findSingleExam(userexam.getExamid());
		ActionContext.getContext().put("exam", exam);

		List<Examquestions> qList1 = examService.findExamquestionsAll(
				userexam.getExamid(), "1");
		for (int i = 0; i < qList1.size(); i++) {
			qList1.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList1.get(i)
									.getQid()));
			qList1.get(i).setUserexamanswers(
					userexamanswerService.findUserexamanswerAllByTqid(qList1
							.get(i).getQid()));

			// 获取正确答案的内容
			String isRight = "";
			for (Testanswer answer : qList1.get(i).getAnswers()) {
				if ("Y".equals(answer.getIsRight())) {
					isRight = answer.getAnswerInfo();
					break;
				}
			}
			qList1.get(i).setIsRight(isRight);
			// 获取学生答案的内容
			String userAnswer = "";
			for (Userexamanswer answer : qList1.get(i).getUserexamanswers()) {
				if ("Y".equals(answer.getIsRight())) {
					userAnswer = answer.getTaname();
					break;
				}
			}
			qList1.get(i).setUserAnswer(userAnswer);
			// 该题分数
			if (isRight.equals(userAnswer) && !"".equals(isRight)) {
				qList1.get(i).setQscore(exam.getDanscore());
			} else {
				qList1.get(i).setQscore(0);
			}

			// 更新该题成绩
			score += qList1.get(i).getQscore();
		}
		ActionContext.getContext().put("qList1", qList1);
		ActionContext.getContext().put("qList1_size", qList1.size());

		List<Examquestions> qList2 = examService.findExamquestionsAll(
				userexam.getExamid(), "2");
		for (int i = 0; i < qList2.size(); i++) {
			qList2.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList2.get(i)
									.getQid()));
			qList2.get(i).setUserexamanswers(
					userexamanswerService.findUserexamanswerAllByTqid(qList2
							.get(i).getQid()));

			// 获取正确答案的内容
			String isRight = "";
			for (Testanswer answer : qList2.get(i).getAnswers()) {
				if ("Y".equals(answer.getIsRight())) {
					isRight += answer.getAnswerInfo() + ",";
				}
			}
			qList2.get(i).setIsRight(isRight);

			// 获取学生答案的内容
			String userAnswer = "";
			for (Userexamanswer answer : qList2.get(i).getUserexamanswers()) {
				if ("Y".equals(answer.getIsRight())) {
					userAnswer += answer.getTaname() + ",";
				}
			}
			qList2.get(i).setUserAnswer(userAnswer);
			// 该题分数
			if (isRight.equals(userAnswer) && !"".equals(isRight)) {
				qList2.get(i).setQscore(exam.getDuoscore());
			} else {
				qList2.get(i).setQscore(0);
			}

			// 更新该题成绩
			score += qList2.get(i).getQscore();
		}
		ActionContext.getContext().put("qList2", qList2);
		ActionContext.getContext().put("qList2_size", qList2.size());

		List<Examquestions> qList3 = examService.findExamquestionsAll(
				userexam.getExamid(), "3");
		for (int i = 0; i < qList3.size(); i++) {
			qList3.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList3.get(i)
									.getQid()));
			qList3.get(i).setUserexamanswers(
					userexamanswerService.findUserexamanswerAllByTqid(qList3
							.get(i).getQid()));

			// 获取正确答案的内容
			String isRight = "";
			for (Testanswer answer : qList3.get(i).getAnswers()) {
				isRight = answer.getIsRight();
			}
			qList3.get(i).setIsRight(isRight);

			// 获取学生答案的内容
			String userAnswer = "";
			for (Userexamanswer answer : qList3.get(i).getUserexamanswers()) {
				userAnswer = answer.getIsRight();
			}
			qList3.get(i).setUserAnswer(userAnswer);
			// 该题分数
			if (isRight.equals(userAnswer) && !"".equals(isRight)) {
				qList3.get(i).setQscore(exam.getPanduanscore());
			} else {
				qList3.get(i).setQscore(0);
			}

			// 更新该题成绩
			score += qList3.get(i).getQscore();
		}
		ActionContext.getContext().put("qList3", qList3);
		ActionContext.getContext().put("qList3_size", qList3.size());

		List<Examquestions> qList4 = examService.findExamquestionsAll(
				userexam.getExamid(), "4");
		for (int i = 0; i < qList4.size(); i++) {
			qList4.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList4.get(i)
									.getQid()));
			qList4.get(i).setUserexamanswers(
					userexamanswerService.findUserexamanswerAllByTqid(qList4
							.get(i).getQid()));
			// 获取正确答案的内容
			String isRight = "";
			for (Testanswer answer : qList4.get(i).getAnswers()) {
				isRight = answer.getAnswerInfo();
			}
			qList4.get(i).setIsRight(isRight);

			// 获取学生答案的内容
			String userAnswer = "";
			for (Userexamanswer answer : qList4.get(i).getUserexamanswers()) {
				userAnswer = answer.getTaname();
				if(answer.getScorce()!=null&&answer.getScorce()!=0){
					// 更新该题成绩
					score += answer.getScorce();
				}
			}
			qList4.get(i).setUserAnswer(userAnswer);
		}
		ActionContext.getContext().put("qList4", qList4);
		ActionContext.getContext().put("qList4_size", qList4.size());

		// 更新成绩
		userexam.setScore(score);
		biz.addOrUpdateUserexam(userexam);
		return "to_user_chengji";
	}

	/**
	 * 学生考试
	 * 
	 * @return
	 */
	public String userexam() {

		Integer uid = (Integer) ActionContext.getContext().getSession()
				.get("uid");
		Exam exam = examService.findSingleExam(examid);
		ActionContext.getContext().put("exam", exam);

		User user = userService.findSingleUser(uid);

		Userexam userexam = new Userexam();
		userexam.setBegidflag("Y");
		userexam.setExamid(examid);
		userexam.setExamname(exam.getExamName());
		userexam.setFinishflag("N");
		userexam.setTesttime(new SimpleDateFormat("yyyy-MM-dd hh:mm:ss")
				.format(new Date()));
		userexam.setUid(uid);
		userexam.setUname(user.getUsername());
		biz.addOrUpdateUserexam(userexam);
		List<Examquestions> qList1 = examService.findExamquestionsAll(examid,
				"1");
		for (int i = 0; i < qList1.size(); i++) {
			qList1.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList1.get(i)
									.getQid()));
		}
		ActionContext.getContext().put("qList1", qList1);
		ActionContext.getContext().put("qList1_size", qList1.size());

		List<Examquestions> qList2 = examService.findExamquestionsAll(examid,
				"2");
		for (int i = 0; i < qList2.size(); i++) {
			qList2.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList2.get(i)
									.getQid()));
		}
		ActionContext.getContext().put("qList2", qList2);
		ActionContext.getContext().put("qList2_size", qList2.size());

		List<Examquestions> qList3 = examService.findExamquestionsAll(examid,
				"3");
		for (int i = 0; i < qList3.size(); i++) {
			qList3.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList3.get(i)
									.getQid()));
		}
		ActionContext.getContext().put("qList3", qList3);
		ActionContext.getContext().put("qList3_size", qList3.size());

		List<Examquestions> qList4 = examService.findExamquestionsAll(examid,
				"4");
		for (int i = 0; i < qList4.size(); i++) {
			qList4.get(i)
					.setAnswers(
							testanswerService.findTestanswerAll(qList4.get(i)
									.getQid()));
		}
		ActionContext.getContext().put("qList4", qList4);
		ActionContext.getContext().put("qList4_size", qList4.size());

		ActionContext.getContext().put("userexam", userexam);

		return "user_examquestion_list";

	}

	private Integer userexamid;
	private Userexam userexamobj;
	private Integer examid;
	private Integer score;

	public Integer getUserexamid() {
		return userexamid;
	}

	public void setUserexamid(Integer userexamid) {
		this.userexamid = userexamid;
	}

	public Userexam getUserexamobj() {
		return userexamobj;
	}

	public void setUserexamobj(Userexam userexamobj) {
		this.userexamobj = userexamobj;
	}

	public Integer getExamid() {
		return examid;
	}

	public void setExamid(Integer examid) {
		this.examid = examid;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}
