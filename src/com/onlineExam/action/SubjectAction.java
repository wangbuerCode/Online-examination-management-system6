package com.onlineExam.action;

import java.util.List;

import com.onlineExam.entity.Subject;
import com.onlineExam.service.SubjectService;
import com.opensymphony.xwork2.ActionContext;

public class SubjectAction {

	private SubjectService biz;

	public SubjectService getBiz() {
		return biz;
	}

	public void setBiz(SubjectService biz) {
		this.biz = biz;
	}

	public String findSubjectAll() {
		List<Subject> subjectList = biz.findSubjectAll();
		ActionContext.getContext().put("subjectList", subjectList);
		return "to_subject_list";
	}

	public String beforeAddOrUpdateSubject() {
		subjectobj = new Subject();
		Subject subject = new Subject();
		if (subjectid != null && !subjectid.equals("")) {
			subject = biz.findSingleSubject(subjectid);
		}
		ActionContext.getContext().put("subject", subject);
		return "to_addOrEditSubject";
	}

	public String addOrUpdateSubject() {
		biz.addOrUpdateSubject(subjectobj);
		return "to_subject_listAction";
	}

	public String deleteSubject() {
		biz.deleteSubject(subjectid);
		return "to_subject_listAction";
	}

	public String deleteSubjects() {
		String[] ids = subjectids.split(",");
		if (ids.length > 0) {
			for (String id : ids) {
				biz.deleteSubject(Integer.parseInt(id.trim()));
			}
		}
		return "to_subject_listAction";
	}

	private Integer subjectid;
	private String subjectids;
	private Subject subjectobj;

	public Integer getSubjectid() {
		return subjectid;
	}

	public void setSubjectid(Integer Subjectid) {
		this.subjectid = Subjectid;
	}

	public Subject getSubjectobj() {
		return subjectobj;
	}

	public void setSubjectobj(Subject Subjectobj) {
		this.subjectobj = Subjectobj;
	}

	public String getSubjectids() {
		return subjectids;
	}

	public void setSubjectids(String subjectids) {
		this.subjectids = subjectids;
	}

}
