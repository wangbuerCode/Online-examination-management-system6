package com.onlineExam.action;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.alibaba.fastjson.JSON;
import com.onlineExam.entity.Classes;
import com.onlineExam.service.ClassesService;
import com.opensymphony.xwork2.ActionContext;

public class ClassesAction {

	private ClassesService biz;

	public ClassesService getBiz() {
		return biz;
	}

	public void setBiz(ClassesService biz) {
		this.biz = biz;
	}

	public String findClassesAll() {
		List<Classes> classesList = biz.findClassesAll(collegeid);
		ActionContext.getContext().put("classesList", classesList);
		
		return "to_classes_list";
	}

	public String beforeAddOrUpdateClasses() {
		classesobj = new Classes();
		Classes classes = new Classes();
		if (classesid != null && !classesid.equals("")) {
			classes = biz.findSingleClasses(classesid);
		}
		ActionContext.getContext().put("classes", classes);
		return "to_addOrEditClasses";
	}

	public String addOrUpdateClasses() {
		biz.addOrUpdateClasses(classesobj);
		return "to_classes_listAction";
	}

	public String deleteClasses() {
		biz.deleteClasses(classesid);
		return "to_classes_listAction";
	}

	public String deleteClassess() {
		String[] ids = classesids.split(",");
		if (ids.length > 0) {
			for (String id : ids) {
				biz.deleteClasses(Integer.parseInt(id.trim()));
			}
		}
		return "to_classes_listAction";
	}
	
	public void findClassesById() {
		List<Classes> classes = biz.findClassesAll(collegeid);
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		
		PrintWriter out = null;

		try{
			out = response.getWriter();
			out.write(JSON.toJSONString(classes));
		}catch(Exception  e){
			e.printStackTrace();
		}
	}

	private Integer classesid;
	private String classesids;
	private Classes classesobj;

	private Integer collegeid;

	public Integer getClassesid() {
		return classesid;
	}

	public void setClassesid(Integer classesid) {
		this.classesid = classesid;
	}

	public String getClassesids() {
		return classesids;
	}

	public void setClassesids(String classesids) {
		this.classesids = classesids;
	}

	public Classes getClassesobj() {
		return classesobj;
	}

	public void setClassesobj(Classes classesobj) {
		this.classesobj = classesobj;
	}

	public Integer getCollegeid() {
		return collegeid;
	}

	public void setCollegeid(Integer collegeid) {
		this.collegeid = collegeid;
	}

}
