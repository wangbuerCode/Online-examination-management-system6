package com.onlineExam.action;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Random;

import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

public class ImageCodeAction {

	public void getImageCode() throws IOException {
		HttpServletRequest request = ServletActionContext.getRequest();
		HttpServletResponse response = ServletActionContext.getResponse();

		int width = 30 * 4;
		int height = 30;

		BufferedImage bufferedImage = new BufferedImage(width, height,
				BufferedImage.TYPE_INT_RGB);

		Graphics graphics = bufferedImage.getGraphics();

		graphics.setColor(new Color(236, 245, 250));
		graphics.fillRect(0, 0, width, height);

		graphics.setColor(new Color(167, 181, 188));
		graphics.drawRect(0, 0, width - 1, height - 1);

		graphics.setColor(Color.RED);
		graphics.setFont(new Font("宋体", Font.BOLD, 30));

		Graphics2D graphics2d = (Graphics2D) graphics;
		String s = "1234567890";
		Random random = new Random();

		String msg = "";
		int x = 5;
		for (int i = 0; i < 4; i++) {
			int index = random.nextInt(s.length());
			String content = String.valueOf(s.charAt(index));
			msg += content;
			double theta = random.nextInt(30) * Math.PI / 180;
			graphics2d.rotate(theta, x, 18);
			graphics2d.drawString(content, x, 18);
			graphics2d.rotate(-theta, x, 18);
			x += 25;
		}

		request.getSession().setAttribute("imageCode", msg);

		graphics.setColor(Color.GRAY);
		for (int i = 0; i < 4; i++) {
			int x1 = random.nextInt(width);
			int x2 = random.nextInt(width);

			int y1 = random.nextInt(height);
			int y2 = random.nextInt(height);
			graphics.drawLine(x1, y1, x2, y2);
		}

		graphics.dispose();

		ImageIO.write(bufferedImage, "jpg", response.getOutputStream());

	}
}
