package com.onlineExam.action;

import java.util.List;

import com.onlineExam.entity.City;
import com.onlineExam.entity.Province;
import com.onlineExam.entity.User;
import com.onlineExam.service.DictService;
import com.onlineExam.service.UserService;
import com.opensymphony.xwork2.ActionContext;

public class UserAction {

	private UserService biz;

	public UserService getBiz() {
		return biz;
	}

	public void setBiz(UserService biz) {
		this.biz = biz;
	}

	public String login() {
		User user = biz.login(username, pwd, role);
		if (user == null) {
			ActionContext.getContext().put("msg", "该用户不存在，请重新登录！");
			return "reLogin";
		} else {
			ActionContext.getContext().getSession().put("uname", username);
			ActionContext.getContext().getSession().put("uid", user.getId());
			ActionContext.getContext().getSession()
					.put("roler", user.getRole());
			ActionContext.getContext().put("msg", "");
			return "to_index";
		}
	}

	public String logout() {
		ActionContext.getContext().getSession().put("uname", null);
		ActionContext.getContext().getSession().put("uid", null);
		ActionContext.getContext().getSession().put("roler", null);
		ActionContext.getContext().put("msg", null);
		return "reLogin";
	}

	private String uid;
	private String username;
	private String pwd;
	private String role;

	public String getUid() {
		return uid;
	}

	public void setUid(String uid) {
		this.uid = uid;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	// --------------用户管理---------------

	private DictService dictService;

	public DictService getDictService() {
		return dictService;
	}

	public void setDictService(DictService dictService) {
		this.dictService = dictService;
	}

	public String findUserAll() {
		List<User> userList = biz.findUserAll(role);
		ActionContext.getContext().put("userList", userList);
		return "to_user_list";
	}

	public String beforeAddOrUpdateUser() {
		userobj = new User();
		User user = new User();
		if (userid != null && !userid.equals("")) {
			user = biz.findSingleUser(userid);
		}
		ActionContext.getContext().put("user", user);

		List<Province> provinces = dictService.findProvinces();
		ActionContext.getContext().put("provinces", provinces);

		return "to_addOrEditUser";
	}

	public String addOrUpdateUser() {
		biz.addOrUpdateUser(userobj);
		return "to_user_listAction";
	}

	public String deleteUser() {
		biz.deleteUser(userid);
		return "to_user_listAction";
	}

	public String deleteUsers() {
		String[] ids = userids.split(",");
		if(ids.length>0){
			for (String id : ids) {
				biz.deleteUser(Integer.parseInt(id.trim()));
			}
		}
		return "to_user_listAction";
	}

	
	public String findUser() {
		User user = biz.findSingleUser(userid);
		ActionContext.getContext().put("user", user);
		
		Province province = dictService.findProvinceById(Integer.parseInt(user.getProvince()));
		City city = dictService.findCityById(Integer.parseInt(user.getCity()));

		ActionContext.getContext().put("pname", province.getPname());
		ActionContext.getContext().put("cname", city.getCname());
		return "to_userInfo_admin";
	}
	
	public String findUserInfo(){
		Integer uid = (Integer)ActionContext.getContext().getSession().get("uid");
		User user = biz.findSingleUser(uid);
		ActionContext.getContext().put("user", user);
		
		List<Province> provinces = dictService.findProvinces();
		ActionContext.getContext().put("provinces", provinces);
		
		Province province = dictService.findProvinceById(Integer.parseInt(user.getProvince()));
		City city = dictService.findCityById(Integer.parseInt(user.getCity()));
		ActionContext.getContext().put("pname", province.getPname());
		ActionContext.getContext().put("cname", city.getCname());
		
		return "to_userInfo";
	}
	
	
	public String saveUserInfo(){
		biz.addOrUpdateUser(userobj);
		return "to_userInfo_action";
	}
	
	private Integer userid;
	private String userids;

	private User userobj;

	public Integer getUserid() {
		return userid;
	}

	public void setUserid(Integer userid) {
		this.userid = userid;
	}

	public User getUserobj() {
		return userobj;
	}

	public void setUserobj(User userobj) {
		this.userobj = userobj;
	}

	public String getUserids() {
		return userids;
	}

	public void setUserids(String userids) {
		this.userids = userids;
	}

}
