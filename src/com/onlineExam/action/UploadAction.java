package com.onlineExam.action;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.onlineExam.util.FileUploadUtil;

public class UploadAction {

	public void upLoade() {
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");

		String path = ServletActionContext.getRequest().getSession()
				.getServletContext().getRealPath("/")
				+ "file\\";

		String[] a = fileFileName.split("\\.");
		String filename = (new Date()).getTime() + "." + a[1];
		FileUploadUtil.uploadFile(path + filename, file);

		PrintWriter out = null;

		try {
			out = response.getWriter();
			out.write("file/" + filename);

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			out.flush();
			out.close();
		}
	}

	private File file;
	private String fileContentType;
	private String fileFileName;

	public File getFile() {
		return file;
	}

	public void setFile(File file) {
		this.file = file;
	}

	public String getFileContentType() {
		return fileContentType;
	}

	public void setFileContentType(String fileContentType) {
		this.fileContentType = fileContentType;
	}

	public String getFileFileName() {
		return fileFileName;
	}

	public void setFileFileName(String fileFileName) {
		this.fileFileName = fileFileName;
	}

}
