package com.onlineExam.action;

import java.util.ArrayList;
import java.util.List;

import com.onlineExam.entity.Subject;
import com.onlineExam.entity.Testanswer;
import com.onlineExam.entity.Testquestions;
import com.onlineExam.service.SubjectService;
import com.onlineExam.service.TestanswerService;
import com.onlineExam.service.TestquestionsService;
import com.opensymphony.xwork2.ActionContext;

public class TestquestionsAction {

	private TestquestionsService biz;
	private TestanswerService testanswerService;
	private SubjectService subjectService;

	public TestquestionsService getBiz() {
		return biz;
	}

	public void setBiz(TestquestionsService biz) {
		this.biz = biz;
	}

	public TestanswerService getTestanswerService() {
		return testanswerService;
	}

	public void setTestanswerService(TestanswerService testanswerService) {
		this.testanswerService = testanswerService;
	}

	public SubjectService getSubjectService() {
		return subjectService;
	}

	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	public String findTestquestionsAll() {
		List<Testquestions> testquestionsList = biz.findTestquestionsAll(testtype);
		ActionContext.getContext().put("testquestionsList", testquestionsList);
		return "to_testquestions_list";
	}

	public String beforeAddOrUpdateTestquestions() {
		testquestionsobj = new Testquestions();
		Testquestions testquestions = new Testquestions();
		if (testquestionsid != null && !testquestionsid.equals("")) {
			testquestions = biz.findSingleTestquestions(testquestionsid);
			List<Testanswer> answers = testanswerService.findTestanswerAll(testquestionsid);
			ActionContext.getContext().put("answers", answers);
		}
		ActionContext.getContext().put("testquestions", testquestions);

		List<Subject> subjects = subjectService.findSubjectAll();
		ActionContext.getContext().put("subjects", subjects);

		return "to_addOrEditTestquestions";
	}

	public String addOrUpdateTestquestions() {
		try {
			if(testquestionsobj.getId()!=null){
				// 删除当前题目的所有答案
				testanswerService.deleteTestanswerByTestquestionsid(testquestionsobj.getId());
			}
			Subject subject = subjectService.findSingleSubject(testquestionsobj.getSid());
			testquestionsobj.setSname(subject.getSubjectName());
			biz.addOrUpdateTestquestions(testquestionsobj);
			
			for (int i = 0; i < testanswers.size(); i++) {
				Testanswer testanswer = testanswers.get(i);
				testanswer.setId(null);
				testanswer.setTqid(testquestionsobj.getId());
				testanswerService.addOrUpdateTestanswer(testanswer);
			}
			
			testanswers = new ArrayList<Testanswer>();
		} catch (Exception e) {
			e.printStackTrace();
		}

		return "to_testquestions_listAction";
	}

	public String deleteTestquestions() {
		biz.deleteTestquestions(testquestionsid);
		testanswerService.deleteTestanswerByTestquestionsid(testquestionsid);
		return "to_testquestions_listAction";
	}

	public String deletetesTquestionss() {
		String[] ids = testquestionsids.split(",");
		if (ids.length > 0) {
			for (String id : ids) {
				biz.deleteTestquestions(Integer.parseInt(id.trim()));
				// 删除答案
				testanswerService.deleteTestanswerByTestquestionsid(Integer.parseInt(id.trim()));
			}
		}
		return "to_testquestions_listAction";
	}

	private Integer testquestionsid;
	private Testquestions testquestionsobj;
	private String testquestionsids;
	private List<Testanswer> testanswers;

	private String testtype;

	public Integer getTestquestionsid() {
		return testquestionsid;
	}

	public void setTestquestionsid(Integer testquestionsid) {
		this.testquestionsid = testquestionsid;
	}

	public Testquestions getTestquestionsobj() {
		return testquestionsobj;
	}

	public void setTestquestionsobj(Testquestions testquestionsobj) {
		this.testquestionsobj = testquestionsobj;
	}

	public String getTestquestionsids() {
		return testquestionsids;
	}

	public void setTestquestionsids(String testquestionsids) {
		this.testquestionsids = testquestionsids;
	}

	public List<Testanswer> getTestanswers() {
		return testanswers;
	}

	public void setTestanswers(List<Testanswer> testanswers) {
		this.testanswers = testanswers;
	}

	public String getTesttype() {
		return testtype;
	}

	public void setTesttype(String testtype) {
		this.testtype = testtype;
	}

}
