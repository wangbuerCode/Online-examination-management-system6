package com.onlineExam.action;

import java.io.PrintWriter;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.struts2.ServletActionContext;

import com.alibaba.fastjson.JSON;
import com.onlineExam.entity.City;
import com.onlineExam.service.DictService;

public class DictAction {

	private DictService biz;

	public DictService getBiz() {
		return biz;
	}

	public void setBiz(DictService biz) {
		this.biz = biz;
	}

	public void findCitiesByPid() {
		List<City> cities = biz.findCitiesByPid(pid);
		HttpServletResponse response = ServletActionContext.getResponse();
		response.setCharacterEncoding("utf-8");
		
		PrintWriter out = null;

		try{
			out = response.getWriter();
			out.write(JSON.toJSONString(cities));
		}catch(Exception  e){
			e.printStackTrace();
		}
	}

	private Integer pid;

	public Integer getPid() {
		return pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

}
