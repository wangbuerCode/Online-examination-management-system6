package com.onlineExam.action;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import com.onlineExam.entity.Classes;
import com.onlineExam.entity.College;
import com.onlineExam.entity.Exam;
import com.onlineExam.entity.Examquestions;
import com.onlineExam.entity.Subject;
import com.onlineExam.entity.Testquestions;
import com.onlineExam.entity.Userexam;
import com.onlineExam.service.ClassesService;
import com.onlineExam.service.CollegeService;
import com.onlineExam.service.ExamService;
import com.onlineExam.service.SubjectService;
import com.onlineExam.service.TestquestionsService;
import com.onlineExam.service.UserexamService;
import com.opensymphony.xwork2.ActionContext;

public class ExamAction {

	private ExamService biz;
	private SubjectService subjectService;
	private TestquestionsService testquestionsService;
	private UserexamService userexamService;
	private CollegeService collegeService;
	private ClassesService classesService;

	public ExamService getBiz() {
		return biz;
	}

	public void setBiz(ExamService biz) {
		this.biz = biz;
	}

	public SubjectService getSubjectService() {
		return subjectService;
	}

	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}

	public TestquestionsService getTestquestionsService() {
		return testquestionsService;
	}

	public void setTestquestionsService(
			TestquestionsService testquestionsService) {
		this.testquestionsService = testquestionsService;
	}

	public UserexamService getUserexamService() {
		return userexamService;
	}

	public void setUserexamService(UserexamService userexamService) {
		this.userexamService = userexamService;
	}

	public CollegeService getCollegeService() {
		return collegeService;
	}

	public void setCollegeService(CollegeService collegeService) {
		this.collegeService = collegeService;
	}

	public ClassesService getClassesService() {
		return classesService;
	}

	public void setClassesService(ClassesService classesService) {
		this.classesService = classesService;
	}

	public String findExamAll() throws Exception {
		try {
			Integer uid = (Integer)ActionContext.getContext().getSession().get("uid");
			String roler = (String)ActionContext.getContext().getSession().get("roler");
			List<Userexam> userexams = null;
			if("3".equals(roler)){
				userexams = userexamService.findUserexamAllByUid(uid);	
			}
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date now = new Date();

			List<Exam> examList = biz.findExamAll();
			for (int i = 0; i < examList.size(); i++) {
				boolean flag = false;
				if("3".equals(roler)){
					userexams = userexamService.findUserexamAllByUid(uid);	
					for (Userexam exam : userexams) {
						if (exam.getExamid().intValue()==examList.get(i).getId().intValue()) {
							examList.remove(i);
							i--;
							flag=true;
							break;
						}
					}
					
					if(!flag){
						Date sdate = sdf.parse(examList.get(i).getSdate());
						Date edate = sdf.parse(examList.get(i).getEdate());

						if (now.before(sdate)) {
							examList.get(i).setFlag("未开始");
						} else if (now.after(edate)) {
							examList.get(i).setFlag("已结束");
						} else {
							examList.get(i).setFlag("进行中");
						}
					}
				}else{
					Date sdate = sdf.parse(examList.get(i).getSdate());
					Date edate = sdf.parse(examList.get(i).getEdate());

					if (now.before(sdate)) {
						examList.get(i).setFlag("未开始");
					} else if (now.after(edate)) {
						examList.get(i).setFlag("已结束");
					} else {
						examList.get(i).setFlag("进行中");
					}
				}
			}
			ActionContext.getContext().put("examList", examList);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return "to_exam_list";
	}

	public String beforeAddOrUpdateExam() {
		examobj = new Exam();
		Exam exam = new Exam();
		if (examid != null && !examid.equals("")) {
			exam = biz.findSingleExam(examid);
		}
		ActionContext.getContext().put("exam", exam);

		List<Subject> subjects = subjectService.findSubjectAll();
		ActionContext.getContext().put("subjects", subjects);
		
		List<College> colleges = collegeService.findCollegeAll();
		ActionContext.getContext().put("colleges", colleges);
		
		return "to_addOrEditExam";
	}

	public String addOrUpdateExam() {

		Subject subject = subjectService.findSingleSubject(examobj.getSid());
		examobj.setSname(subject.getSubjectName());
		
		College college = collegeService.findSingleCollege(examobj.getCollegeid());
		Classes classes = classesService.findSingleClasses(examobj.getClassesid());
		examobj.setCollegename(college.getCollegename());
		examobj.setClassesname(classes.getClassesname());
		
		List<Testquestions> dans = testquestionsService
				.findTestquestionsAllBySidAndType(examobj.getSid(), "1"); // 获取有效的单选题
		List<Testquestions> duos = testquestionsService
				.findTestquestionsAllBySidAndType(examobj.getSid(), "2"); // 获取有效的多选题
		List<Testquestions> panduans = testquestionsService
				.findTestquestionsAllBySidAndType(examobj.getSid(), "3"); // 获取有效的判断
		List<Testquestions> zizhus = testquestionsService
				.findTestquestionsAllBySidAndType(examobj.getSid(), "4"); // 获取有效的自主
		if (dans == null || dans.size() <= examobj.getDan().intValue()) {
			examobj.setDan(dans.size());
		}
		if (duos == null || duos.size() <= examobj.getDuo().intValue()) {
			examobj.setDuo(duos.size());
		}
		if (panduans == null
				|| panduans.size() <= examobj.getPanduan().intValue()) {
			examobj.setPanduan(panduans.size());
		}
		if (zizhus == null || zizhus.size() <= examobj.getZizhu().intValue()) {
			examobj.setZizhu(zizhus.size());
		}
		biz.addOrUpdateExam(examobj);

			// 保存试题
			if (dans == null || dans.size() <= examobj.getDan().intValue()) {
				for (Testquestions obj : dans) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(obj.getId());
					examquestions.setQscore(obj.getScore());
					examquestions.setQtitle(obj.getTesttitle());
					examquestions.setQtype(obj.getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			} else {
				int[] iarr = randomCommon(1, dans.size(), examobj.getDan());
				for (int i : iarr) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(dans.get(i - 1).getId());
					examquestions.setQscore(dans.get(i - 1).getScore());
					examquestions.setQtitle(dans.get(i - 1).getTesttitle());
					examquestions.setQtype(dans.get(i - 1).getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			}
			if (duos == null || duos.size() <= examobj.getDuo().intValue()) {
				for (Testquestions obj : duos) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(obj.getId());
					examquestions.setQscore(obj.getScore());
					examquestions.setQtitle(obj.getTesttitle());
					examquestions.setQtype(obj.getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			} else {
				int[] iarr = randomCommon(1, duos.size(), examobj.getDuo());
				for (int i : iarr) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(duos.get(i - 1).getId());
					examquestions.setQscore(duos.get(i - 1).getScore());
					examquestions.setQtitle(duos.get(i - 1).getTesttitle());
					examquestions.setQtype(duos.get(i - 1).getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			}
			if (panduans == null
					|| panduans.size() <= examobj.getPanduan().intValue()) {
				for (Testquestions obj : panduans) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(obj.getId());
					examquestions.setQscore(obj.getScore());
					examquestions.setQtitle(obj.getTesttitle());
					examquestions.setQtype(obj.getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			} else {
				int[] iarr = randomCommon(1, panduans.size(),
						examobj.getPanduan());
				for (int i : iarr) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(panduans.get(i - 1).getId());
					examquestions.setQscore(panduans.get(i - 1).getScore());
					examquestions.setQtitle(panduans.get(i - 1).getTesttitle());
					examquestions.setQtype(panduans.get(i - 1).getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			}
			if (zizhus == null
					|| zizhus.size() <= examobj.getZizhu().intValue()) {
				for (Testquestions obj : zizhus) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(obj.getId());
					examquestions.setQscore(obj.getScore());
					examquestions.setQtitle(obj.getTesttitle());
					examquestions.setQtype(obj.getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			} else {
				int[] iarr = randomCommon(1, zizhus.size(), examobj.getZizhu());
				for (int i : iarr) {
					Examquestions examquestions = new Examquestions();
					examquestions.setEid(examobj.getId());
					examquestions.setQid(zizhus.get(i - 1).getId());
					examquestions.setQscore(zizhus.get(i - 1).getScore());
					examquestions.setQtitle(zizhus.get(i - 1).getTesttitle());
					examquestions.setQtype(zizhus.get(i - 1).getTesttype());
					biz.addOrUpdateExamquestions(examquestions);
				}
			}
		return "to_exam_listAction";
	}

	/**
	 * 随机指定范围内N个不重复的数
	 * 
	 * @param min
	 *            指定范围最小值
	 * @param max
	 *            指定范围最大值
	 * @param n
	 *            随机数个数
	 */
	public int[] randomCommon(int min, int max, int n) {

		if (n > (max - min + 1) || max < min) {
			return null;
		}
		int[] result = new int[n];
		int count = 0;
		while (count < n) {
			int num = (int) (Math.random() * (max - min)) + min;
			boolean flag = true;
			for (int j = 0; j < n; j++) {
				if (num == result[j]) {
					flag = false;
					break;
				}
			}
			if (flag) {
				result[count] = num;
				count++;
			}
		}
		return result;
	}

	public String deleteExam() {
		biz.deleteExam(examid);
		biz.deleteExamquestionsByEid(examid);
		return "to_exam_listAction";
	}

	public String deleteExams() {
		String[] ids = examids.split(",");
		if (ids.length > 0) {
			for (String id : ids) {
				biz.deleteExam(Integer.parseInt(id.trim()));
				biz.deleteExamquestionsByEid(Integer.parseInt(id.trim()));
			}
		}
		return "to_exam_listAction";
	}

	public String findExamquestionsAll() {
		List<Examquestions> examquestionsList = biz.findExamquestionsAll(examid,"");
		ActionContext.getContext().put("examquestionsList", examquestionsList);
		return "to_examquestions_list";
	}

	private Integer examid;
	private String examids;
	private Exam examobj;

	private Integer dan;
	private Integer duo;
	private Integer panduan;
	private Integer zizhu;

	public Integer getExamid() {
		return examid;
	}

	public void setExamid(Integer examid) {
		this.examid = examid;
	}

	public String getExamids() {
		return examids;
	}

	public void setExamids(String examids) {
		this.examids = examids;
	}

	public Exam getExamobj() {
		return examobj;
	}

	public void setExamobj(Exam examobj) {
		this.examobj = examobj;
	}

	public Integer getDan() {
		return dan;
	}

	public void setDan(Integer dan) {
		this.dan = dan;
	}

	public Integer getDuo() {
		return duo;
	}

	public void setDuo(Integer duo) {
		this.duo = duo;
	}

	public Integer getPanduan() {
		return panduan;
	}

	public void setPanduan(Integer panduan) {
		this.panduan = panduan;
	}

	public Integer getZizhu() {
		return zizhu;
	}

	public void setZizhu(Integer zizhu) {
		this.zizhu = zizhu;
	}

}
