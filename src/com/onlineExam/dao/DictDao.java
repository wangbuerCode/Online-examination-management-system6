package com.onlineExam.dao;

import java.util.List;

import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.City;
import com.onlineExam.entity.Province;

public class DictDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}

	public List<Province> findProvinces() {
		return ht.find("from Province order by pid asc");
	}

	public List<City> findCitiesByPid(Integer pid) {
		return ht.find("from City where pid="+pid+" order by cid asc");
	}

	public Province findProvinceById(Integer pid) {
		return ht.get(Province.class, pid);
	}

	public City findCityById(Integer cid) {
		return ht.get(City.class, cid);
	}
}
