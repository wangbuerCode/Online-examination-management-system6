package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Testquestions;

public class TestquestionsDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Testquestions> findTestquestionsAll(String testtype) {
		return ht.find("from Testquestions where testtype='"+testtype+"' order by id asc");
	}

	public Testquestions findSingleTestquestions(Integer id) {
		Testquestions Testquestions = ht.get(Testquestions.class, id);
		return Testquestions;
	}

	public void addOrUpdateTestquestions(Testquestions obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteTestquestions(Integer id) {
		String sql = "delete from testquestions where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public List<Testquestions> findTestquestionsAllBySidAndType(Integer sid, String qtype) {
		System.out.println(sid+":"+qtype);
		try {
			return ht.find("from Testquestions where testtype='"+qtype+"' and sid="+sid+" order by id desc");
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
}
