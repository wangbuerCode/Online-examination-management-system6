package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.User;

public class UserDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}

	public List<User> findUserAll(String role) {
		List<User> list = ht.find("from User t where role='"+role+"' order by t.id desc ");
		return list;
	}

	public boolean addOrUpdateUser(User userobj) {
		if (null == userobj.getId()) {
			ht.save(userobj);
		} else {
			ht.update(userobj);
		}
		return true;
	}

	public User findSingleUser(Integer uid) {
		User user = ht.get(User.class, uid);
		return user;
	}

	public void updatePwd(String uid, String newpwd) {
		String sql = "update user set pwd = '" + newpwd + "' where id=" + uid;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public User login(String username, String pwd, String role) {
		
		List<User> list = ht.find("from User where username='" + username + "' and pwd='" + pwd + "' and role='"+role+"'");

		if (list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}

	public void deleteUser(Integer userid) {
		String sql = "delete from `user` where id=" + userid;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
	}
}
