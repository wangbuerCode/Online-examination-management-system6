package com.onlineExam.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Userexam;

public class UserexamDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Userexam> findUserexamAll() {
		return ht.find("from Userexam order by id desc");
	}

	public Userexam findSingleUserexam(Integer id) {
		Userexam Userexam = ht.get(Userexam.class, id);
		return Userexam;
	}

	public void addOrUpdateUserexam(Userexam obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteUserexam(Integer id) {
		String sql = "delete from userexam where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public List<Userexam> findUserexamAllByUid(Integer uid) {
		return ht.find("from Userexam where uid="+uid+" order by id desc");
	}

	public List<Object[]> findUserexamAllTj() {
		String sql = "select score,count(1) from userexam group by score";
		Session session = null;
		List<Object[]> list = null;
		try {
			session = ht.getSessionFactory().openSession();
			list = session.createSQLQuery(sql).list();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		return list;
	}
}
