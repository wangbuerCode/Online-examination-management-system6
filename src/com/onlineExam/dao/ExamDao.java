package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Exam;
import com.onlineExam.entity.Examquestions;

public class ExamDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Exam> findExamAll() {
		return ht.find("from Exam order by id desc");
	}

	public Exam findSingleExam(Integer id) {
		return ht.get(Exam.class, id);
	}

	public void addOrUpdateExam(Exam obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteExam(Integer id) {
		String sql = "delete from exam where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public List<Examquestions> findExamquestionsAll(Integer eid, String type) {
		String sql = "from Examquestions where eid="+eid;
		if(type!=null&&!"".equals(type)){
			sql+=" and qtype='"+type+"'";
		}
		sql+=" order by qtype asc";
		return ht.find(sql);
	}

	public Examquestions findSingleExamquestions(Integer id) {
		return ht.get(Examquestions.class, id);
	}

	public void addOrUpdateExamquestions(Examquestions obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteExamquestions(Integer id) {
		String sql = "delete from examquestions where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
	}

	public void deleteExamquestionsByEid(Integer eid) {
		String sql = "delete from examquestions where eid=" + eid;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
	}
}
