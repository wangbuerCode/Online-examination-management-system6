package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Classes;

public class ClassesDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Classes> findClassesAll(Integer collegeid) {
		return ht.find("from Classes where collegeid="+collegeid+" order by classesid desc");
	}

	public Classes findSingleClasses(Integer id) {
		Classes Classes = ht.get(Classes.class, id);
		return Classes;
	}

	public void addOrUpdateClasses(Classes obj) {
		if (null == obj.getClassesid()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteClasses(Integer id) {
		String sql = "delete from classes where classesid=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}
}
