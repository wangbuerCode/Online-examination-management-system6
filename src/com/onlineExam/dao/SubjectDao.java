package com.onlineExam.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Subject;

public class SubjectDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Subject> findSubjectAll() {
		return ht.find("from Subject order by id desc");
	}

	public Subject findSingleSubject(Integer id) {
		Subject Subject = ht.get(Subject.class, id);
		return Subject;
	}

	public void addOrUpdateSubject(Subject obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteSubject(Integer id) {
		String sql = "delete from subject where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}
}
