package com.onlineExam.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.dao.DataAccessException;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Userexamanswer;

public class UserexamanswerDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Userexamanswer> findUserexamanswerAll() {
		return ht.find("from Userexamanswer order by id desc");
	}

	public Userexamanswer findSingleUserexamanswer(Integer id) {
		Userexamanswer Userexamanswer = ht.get(Userexamanswer.class, id);
		return Userexamanswer;
	}

	public void addOrUpdateUserexamanswer(Userexamanswer obj) {
		try {
			if (null == obj.getId()) {
				ht.save(obj);
			} else {
				ht.update(obj);
			}
		} catch (DataAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void deleteUserexamanswer(Integer id) {
		String sql = "delete from userexamanswer where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public List<Userexamanswer> findUserexamanswerAllByTqid(Integer tqid) {
		return ht.find("from Userexamanswer where tqid="+tqid+" order by id desc");
	}

	public Userexamanswer findUserexamanswerAllByInfo(Integer userexamid, Integer pid) {
		List<Userexamanswer> list = ht.find("from Userexamanswer where ueid="+userexamid+" and tqid="+pid+" order by id desc");
		if (list!=null&&list.size()>0) {
			return list.get(0);
		}else{
			return null;	
		}
	}
}
