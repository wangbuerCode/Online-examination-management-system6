package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.College;

public class CollegeDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<College> findCollegeAll() {
		return ht.find("from College order by collegeid desc");
	}

	public College findSingleCollege(Integer id) {
		College College = ht.get(College.class, id);
		return College;
	}

	public void addOrUpdateCollege(College obj) {
		if (null == obj.getCollegeid()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteCollege(Integer id) {
		String sql = "delete from college where collegeid=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}
}
