package com.onlineExam.dao;

import java.util.List;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.springframework.orm.hibernate3.HibernateTemplate;

import com.onlineExam.entity.Testanswer;

public class TestanswerDao {

	private HibernateTemplate ht;

	public HibernateTemplate getHt() {
		return ht;
	}

	public void setHt(HibernateTemplate ht) {
		this.ht = ht;
	}
	
	public List<Testanswer> findTestanswerAll(Integer testquestionsid) {
		return ht.find("from Testanswer where tqid="+testquestionsid+" order by id asc");
	}

	public Testanswer findSingleTestanswer(Integer id) {
		Testanswer Testanswer = ht.get(Testanswer.class, id);
		return Testanswer;
	}

	public void addOrUpdateTestanswer(Testanswer obj) {
		if (null == obj.getId()) {
			ht.save(obj);
		} else {
			ht.update(obj);
		}
	}

	public void deleteTestanswer(Integer id) {
		String sql = "delete from testanswer where id=" + id;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
	}

	public void deleteTestanswerByTestquestionsid(int testquestionsid) {
		String sql = "delete from testanswer where tqid=" + testquestionsid;
		Session session = null;
		try {
			session = ht.getSessionFactory().openSession();
			session.createSQLQuery(sql).executeUpdate();
		} catch (HibernateException e) {
			e.printStackTrace();
		} finally {
			session.flush();
			session.close();
		}
		
	}
}
