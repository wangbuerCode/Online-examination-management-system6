package com.onlineExam.entity;

import java.util.List;

/**
 * Examquestions entity. @author MyEclipse Persistence Tools
 */

public class Examquestions implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer eid;
	private Integer qid;
	private String qtitle;
	private String qtype;
	private Integer qscore;

	private List<Testanswer> answers;
	private List<Userexamanswer> userexamanswers;

	private String isRight;
	private String userAnswer;

	// Constructors

	/** default constructor */
	public Examquestions() {
	}

	/** full constructor */
	public Examquestions(Integer eid, Integer qid, String qtitle, String qtype,
			Integer qscore) {
		this.eid = eid;
		this.qid = qid;
		this.qtitle = qtitle;
		this.qtype = qtype;
		this.qscore = qscore;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEid() {
		return this.eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public Integer getQid() {
		return this.qid;
	}

	public void setQid(Integer qid) {
		this.qid = qid;
	}

	public String getQtitle() {
		return this.qtitle;
	}

	public void setQtitle(String qtitle) {
		this.qtitle = qtitle;
	}

	public String getQtype() {
		return this.qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

	public Integer getQscore() {
		return this.qscore;
	}

	public void setQscore(Integer qscore) {
		this.qscore = qscore;
	}

	public List<Testanswer> getAnswers() {
		return answers;
	}

	public void setAnswers(List<Testanswer> answers) {
		this.answers = answers;
	}

	public List<Userexamanswer> getUserexamanswers() {
		return userexamanswers;
	}

	public void setUserexamanswers(List<Userexamanswer> userexamanswers) {
		this.userexamanswers = userexamanswers;
	}

	public String getIsRight() {
		return isRight;
	}

	public void setIsRight(String isRight) {
		this.isRight = isRight;
	}

	public String getUserAnswer() {
		return userAnswer;
	}

	public void setUserAnswer(String userAnswer) {
		this.userAnswer = userAnswer;
	}

}