package com.onlineExam.entity;

/**
 * Testquestions entity. @author MyEclipse Persistence Tools
 */

public class Testquestions implements java.io.Serializable {

	// Fields

	private Integer id;
	private String testtitle;
	private String testtype;
	private Integer sid;
	private String sname;
	private Integer score;

	// Constructors

	/** default constructor */
	public Testquestions() {
	}

	/** full constructor */
	public Testquestions(String testtitle, String testtype, Integer sid,
			String sname, Integer score) {
		this.testtitle = testtitle;
		this.testtype = testtype;
		this.sid = sid;
		this.sname = sname;
		this.score = score;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTesttitle() {
		return this.testtitle;
	}

	public void setTesttitle(String testtitle) {
		this.testtitle = testtitle;
	}

	public String getTesttype() {
		return this.testtype;
	}

	public void setTesttype(String testtype) {
		this.testtype = testtype;
	}

	public Integer getSid() {
		return this.sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getSname() {
		return this.sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}