package com.onlineExam.entity;

/**
 * College entity. @author MyEclipse Persistence Tools
 */

public class College implements java.io.Serializable {

	// Fields

	private Integer collegeid;
	private String collegename;
	private String content;
	private String createyear;
	private String dean;

	// Constructors

	/** default constructor */
	public College() {
	}

	/** full constructor */
	public College(String collegename, String content, String createyear,
			String dean) {
		this.collegename = collegename;
		this.content = content;
		this.createyear = createyear;
		this.dean = dean;
	}

	// Property accessors

	public Integer getCollegeid() {
		return this.collegeid;
	}

	public void setCollegeid(Integer collegeid) {
		this.collegeid = collegeid;
	}

	public String getCollegename() {
		return this.collegename;
	}

	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}

	public String getContent() {
		return this.content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCreateyear() {
		return this.createyear;
	}

	public void setCreateyear(String createyear) {
		this.createyear = createyear;
	}

	public String getDean() {
		return this.dean;
	}

	public void setDean(String dean) {
		this.dean = dean;
	}

}