package com.onlineExam.entity;

/**
 * Testanswer entity. @author MyEclipse Persistence Tools
 */

public class Testanswer implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer tqid;
	private String answerInfo;
	private String isRight;
	private String sortFlag;

	// Constructors

	/** default constructor */
	public Testanswer() {
	}

	/** full constructor */
	public Testanswer(Integer tqid, String answerInfo, String isRight,
			String sortFlag) {
		this.tqid = tqid;
		this.answerInfo = answerInfo;
		this.isRight = isRight;
		this.sortFlag = sortFlag;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getTqid() {
		return this.tqid;
	}

	public void setTqid(Integer tqid) {
		this.tqid = tqid;
	}

	public String getAnswerInfo() {
		return this.answerInfo;
	}

	public void setAnswerInfo(String answerInfo) {
		this.answerInfo = answerInfo;
	}

	public String getIsRight() {
		return this.isRight;
	}

	public void setIsRight(String isRight) {
		this.isRight = isRight;
	}

	public String getSortFlag() {
		return this.sortFlag;
	}

	public void setSortFlag(String sortFlag) {
		this.sortFlag = sortFlag;
	}

}