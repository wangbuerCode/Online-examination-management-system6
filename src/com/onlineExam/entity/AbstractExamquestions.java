package com.onlineExam.entity;

/**
 * AbstractExamquestions entity provides the base persistence definition of the
 * Examquestions entity. @author MyEclipse Persistence Tools
 */

public abstract class AbstractExamquestions implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer eid;
	private Integer qid;
	private String qtitle;

	// Constructors

	/** default constructor */
	public AbstractExamquestions() {
	}

	/** full constructor */
	public AbstractExamquestions(Integer eid, Integer qid, String qtitle) {
		this.eid = eid;
		this.qid = qid;
		this.qtitle = qtitle;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEid() {
		return this.eid;
	}

	public void setEid(Integer eid) {
		this.eid = eid;
	}

	public Integer getQid() {
		return this.qid;
	}

	public void setQid(Integer qid) {
		this.qid = qid;
	}

	public String getQtitle() {
		return this.qtitle;
	}

	public void setQtitle(String qtitle) {
		this.qtitle = qtitle;
	}

}