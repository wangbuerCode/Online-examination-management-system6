package com.onlineExam.entity;

/**
 * District entity. @author MyEclipse Persistence Tools
 */

public class District implements java.io.Serializable {

	// Fields

	private Integer did;
	private String dname;
	private Integer cid;

	// Constructors

	/** default constructor */
	public District() {
	}

	/** full constructor */
	public District(Integer did, String dname, Integer cid) {
		this.did = did;
		this.dname = dname;
		this.cid = cid;
	}

	// Property accessors

	public Integer getDid() {
		return this.did;
	}

	public void setDid(Integer did) {
		this.did = did;
	}

	public String getDname() {
		return this.dname;
	}

	public void setDname(String dname) {
		this.dname = dname;
	}

	public Integer getCid() {
		return this.cid;
	}

	public void setCid(Integer cid) {
		this.cid = cid;
	}

}