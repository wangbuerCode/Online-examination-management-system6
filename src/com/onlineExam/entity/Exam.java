package com.onlineExam.entity;

/**
 * Exam entity. @author MyEclipse Persistence Tools
 */

public class Exam implements java.io.Serializable {

	// Fields

	private Integer id;
	private String examName;
	private Integer sid;
	private String sname;
	private String sdate;
	private String edate;
	private String examTime;
	private String remark;
	private Integer dan;
	private Integer duo;
	private Integer panduan;
	private Integer zizhu;
	private Integer danscore;
	private Integer duoscore;
	private Integer panduanscore;
	private Integer zizhuscore;
	private Integer collegeid;
	private String collegename;
	private Integer classesid;
	private String classesname;
	private String flag;

	// Constructors

	/** default constructor */
	public Exam() {
	}

	/** full constructor */
	public Exam(String examName, Integer sid, String sname, String sdate,
			String edate, String examTime, String remark, Integer dan,
			Integer duo, Integer panduan, Integer zizhu, Integer danscore,
			Integer duoscore, Integer panduanscore, Integer zizhuscore,
			Integer collegeid, String collegename, Integer classesid,
			String classesname) {
		this.examName = examName;
		this.sid = sid;
		this.sname = sname;
		this.sdate = sdate;
		this.edate = edate;
		this.examTime = examTime;
		this.remark = remark;
		this.dan = dan;
		this.duo = duo;
		this.panduan = panduan;
		this.zizhu = zizhu;
		this.danscore = danscore;
		this.duoscore = duoscore;
		this.panduanscore = panduanscore;
		this.zizhuscore = zizhuscore;
		this.collegeid = collegeid;
		this.collegename = collegename;
		this.classesid = classesid;
		this.classesname = classesname;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getExamName() {
		return this.examName;
	}

	public void setExamName(String examName) {
		this.examName = examName;
	}

	public Integer getSid() {
		return this.sid;
	}

	public void setSid(Integer sid) {
		this.sid = sid;
	}

	public String getSname() {
		return this.sname;
	}

	public void setSname(String sname) {
		this.sname = sname;
	}

	public String getSdate() {
		return this.sdate;
	}

	public void setSdate(String sdate) {
		this.sdate = sdate;
	}

	public String getEdate() {
		return this.edate;
	}

	public void setEdate(String edate) {
		this.edate = edate;
	}

	public String getExamTime() {
		return this.examTime;
	}

	public void setExamTime(String examTime) {
		this.examTime = examTime;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public Integer getDan() {
		return this.dan;
	}

	public void setDan(Integer dan) {
		this.dan = dan;
	}

	public Integer getDuo() {
		return this.duo;
	}

	public void setDuo(Integer duo) {
		this.duo = duo;
	}

	public Integer getPanduan() {
		return this.panduan;
	}

	public void setPanduan(Integer panduan) {
		this.panduan = panduan;
	}

	public Integer getZizhu() {
		return this.zizhu;
	}

	public void setZizhu(Integer zizhu) {
		this.zizhu = zizhu;
	}

	public Integer getDanscore() {
		return this.danscore;
	}

	public void setDanscore(Integer danscore) {
		this.danscore = danscore;
	}

	public Integer getDuoscore() {
		return this.duoscore;
	}

	public void setDuoscore(Integer duoscore) {
		this.duoscore = duoscore;
	}

	public Integer getPanduanscore() {
		return this.panduanscore;
	}

	public void setPanduanscore(Integer panduanscore) {
		this.panduanscore = panduanscore;
	}

	public Integer getZizhuscore() {
		return this.zizhuscore;
	}

	public void setZizhuscore(Integer zizhuscore) {
		this.zizhuscore = zizhuscore;
	}

	public Integer getCollegeid() {
		return this.collegeid;
	}

	public void setCollegeid(Integer collegeid) {
		this.collegeid = collegeid;
	}

	public String getCollegename() {
		return this.collegename;
	}

	public void setCollegename(String collegename) {
		this.collegename = collegename;
	}

	public Integer getClassesid() {
		return this.classesid;
	}

	public void setClassesid(Integer classesid) {
		this.classesid = classesid;
	}

	public String getClassesname() {
		return this.classesname;
	}

	public void setClassesname(String classesname) {
		this.classesname = classesname;
	}

	public String getFlag() {
		return flag;
	}

	public void setFlag(String flag) {
		this.flag = flag;
	}

}