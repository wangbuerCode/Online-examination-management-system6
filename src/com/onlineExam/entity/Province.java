package com.onlineExam.entity;

/**
 * Province entity. @author MyEclipse Persistence Tools
 */

public class Province implements java.io.Serializable {

	// Fields

	private Integer pid;
	private String pname;

	// Constructors

	/** default constructor */
	public Province() {
	}

	/** full constructor */
	public Province(Integer pid, String pname) {
		this.pid = pid;
		this.pname = pname;
	}

	// Property accessors

	public Integer getPid() {
		return this.pid;
	}

	public void setPid(Integer pid) {
		this.pid = pid;
	}

	public String getPname() {
		return this.pname;
	}

	public void setPname(String pname) {
		this.pname = pname;
	}

}