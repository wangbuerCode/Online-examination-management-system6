package com.onlineExam.entity;

/**
 * Userexamanswer entity. @author MyEclipse Persistence Tools
 */

public class Userexamanswer implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer ueid;
	private String tqid;
	private String tqname;
	private Integer taid;
	private String taname;
	private String isRight;
	private Integer scorce;
	private String qtype;

	// Constructors

	/** default constructor */
	public Userexamanswer() {
	}

	/** full constructor */
	public Userexamanswer(Integer ueid, String tqid, String tqname,
			Integer taid, String taname, String isRight, Integer scorce,
			String qtype) {
		this.ueid = ueid;
		this.tqid = tqid;
		this.tqname = tqname;
		this.taid = taid;
		this.taname = taname;
		this.isRight = isRight;
		this.scorce = scorce;
		this.qtype = qtype;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUeid() {
		return this.ueid;
	}

	public void setUeid(Integer ueid) {
		this.ueid = ueid;
	}

	public String getTqid() {
		return this.tqid;
	}

	public void setTqid(String tqid) {
		this.tqid = tqid;
	}

	public String getTqname() {
		return this.tqname;
	}

	public void setTqname(String tqname) {
		this.tqname = tqname;
	}

	public Integer getTaid() {
		return this.taid;
	}

	public void setTaid(Integer taid) {
		this.taid = taid;
	}

	public String getTaname() {
		return this.taname;
	}

	public void setTaname(String taname) {
		this.taname = taname;
	}

	public String getIsRight() {
		return this.isRight;
	}

	public void setIsRight(String isRight) {
		this.isRight = isRight;
	}

	public Integer getScorce() {
		return this.scorce;
	}

	public void setScorce(Integer scorce) {
		this.scorce = scorce;
	}

	public String getQtype() {
		return this.qtype;
	}

	public void setQtype(String qtype) {
		this.qtype = qtype;
	}

}