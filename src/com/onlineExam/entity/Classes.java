package com.onlineExam.entity;

/**
 * Classes entity. @author MyEclipse Persistence Tools
 */

public class Classes implements java.io.Serializable {

	// Fields

	private Integer classesid;
	private Integer collegeid;
	private String classesname;

	// Constructors

	/** default constructor */
	public Classes() {
	}

	/** full constructor */
	public Classes(Integer collegeid, String classesname) {
		this.collegeid = collegeid;
		this.classesname = classesname;
	}

	// Property accessors

	public Integer getClassesid() {
		return this.classesid;
	}

	public void setClassesid(Integer classesid) {
		this.classesid = classesid;
	}

	public Integer getCollegeid() {
		return this.collegeid;
	}

	public void setCollegeid(Integer collegeid) {
		this.collegeid = collegeid;
	}

	public String getClassesname() {
		return this.classesname;
	}

	public void setClassesname(String classesname) {
		this.classesname = classesname;
	}

}