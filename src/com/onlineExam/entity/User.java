package com.onlineExam.entity;

/**
 * User entity. @author MyEclipse Persistence Tools
 */

public class User implements java.io.Serializable {

	// Fields

	private Integer id;
	private String username;
	private String pwd;
	private String role;
	private String sex;
	private String birdate;
	private String city;
	private String province;
	private String address;
	private String email;
	private String image;
	private String nation;
	private String other;
	private String phone;
	private String college;
	private String degree;
	private String education;
	private String classes;

	// Constructors

	/** default constructor */
	public User() {
	}

	/** full constructor */
	public User(String username, String pwd, String role, String sex,
			String birdate, String city, String province, String address,
			String email, String image, String nation, String other,
			String phone, String college, String degree, String education) {
		this.username = username;
		this.pwd = pwd;
		this.role = role;
		this.sex = sex;
		this.birdate = birdate;
		this.city = city;
		this.province = province;
		this.address = address;
		this.email = email;
		this.image = image;
		this.nation = nation;
		this.other = other;
		this.phone = phone;
		this.college = college;
		this.degree = degree;
		this.education = education;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUsername() {
		return this.username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPwd() {
		return this.pwd;
	}

	public void setPwd(String pwd) {
		this.pwd = pwd;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getSex() {
		return this.sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getBirdate() {
		return this.birdate;
	}

	public void setBirdate(String birdate) {
		this.birdate = birdate;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return this.province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getImage() {
		return this.image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getNation() {
		return this.nation;
	}

	public void setNation(String nation) {
		this.nation = nation;
	}

	public String getOther() {
		return this.other;
	}

	public void setOther(String other) {
		this.other = other;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCollege() {
		return this.college;
	}

	public void setCollege(String college) {
		this.college = college;
	}

	public String getDegree() {
		return this.degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public String getEducation() {
		return this.education;
	}

	public void setEducation(String education) {
		this.education = education;
	}

	public String getClasses() {
		return classes;
	}

	public void setClasses(String classes) {
		this.classes = classes;
	}

}