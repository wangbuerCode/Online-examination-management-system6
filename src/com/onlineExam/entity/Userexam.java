package com.onlineExam.entity;

/**
 * Userexam entity. @author MyEclipse Persistence Tools
 */

public class Userexam implements java.io.Serializable {

	// Fields

	private Integer id;
	private Integer uid;
	private String uname;
	private String testtime;
	private Integer examid;
	private String examname;
	private String begidflag;
	private String finishflag;
	private String correctingflag;
	private Integer correctinguid;
	private String correctinguname;
	private Integer score;

	// Constructors

	/** default constructor */
	public Userexam() {
	}

	/** full constructor */
	public Userexam(Integer uid, String uname, String testtime, Integer examid,
			String examname, String begidflag, String finishflag,
			String correctingflag, Integer correctinguid,
			String correctinguname, Integer score) {
		this.uid = uid;
		this.uname = uname;
		this.testtime = testtime;
		this.examid = examid;
		this.examname = examname;
		this.begidflag = begidflag;
		this.finishflag = finishflag;
		this.correctingflag = correctingflag;
		this.correctinguid = correctinguid;
		this.correctinguname = correctinguname;
		this.score = score;
	}

	// Property accessors

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUid() {
		return this.uid;
	}

	public void setUid(Integer uid) {
		this.uid = uid;
	}

	public String getUname() {
		return this.uname;
	}

	public void setUname(String uname) {
		this.uname = uname;
	}

	public String getTesttime() {
		return this.testtime;
	}

	public void setTesttime(String testtime) {
		this.testtime = testtime;
	}

	public Integer getExamid() {
		return this.examid;
	}

	public void setExamid(Integer examid) {
		this.examid = examid;
	}

	public String getExamname() {
		return this.examname;
	}

	public void setExamname(String examname) {
		this.examname = examname;
	}

	public String getBegidflag() {
		return this.begidflag;
	}

	public void setBegidflag(String begidflag) {
		this.begidflag = begidflag;
	}

	public String getFinishflag() {
		return this.finishflag;
	}

	public void setFinishflag(String finishflag) {
		this.finishflag = finishflag;
	}

	public String getCorrectingflag() {
		return this.correctingflag;
	}

	public void setCorrectingflag(String correctingflag) {
		this.correctingflag = correctingflag;
	}

	public Integer getCorrectinguid() {
		return this.correctinguid;
	}

	public void setCorrectinguid(Integer correctinguid) {
		this.correctinguid = correctinguid;
	}

	public String getCorrectinguname() {
		return this.correctinguname;
	}

	public void setCorrectinguname(String correctinguname) {
		this.correctinguname = correctinguname;
	}

	public Integer getScore() {
		return this.score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

}