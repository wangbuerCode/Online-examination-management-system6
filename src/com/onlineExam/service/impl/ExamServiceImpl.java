package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.ExamDao;
import com.onlineExam.entity.Exam;
import com.onlineExam.entity.Examquestions;
import com.onlineExam.service.ExamService;
@Service
public class ExamServiceImpl implements ExamService {

	private ExamDao dao;

	public ExamDao getDao() {
		return dao;
	}

	public void setDao(ExamDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Exam> findExamAll() {
		return dao.findExamAll();
	}

	@Override
	public Exam findSingleExam(Integer id) {
		return dao.findSingleExam(id);
	}

	@Override
	public void addOrUpdateExam(Exam obj) {
		dao.addOrUpdateExam(obj);
	}

	@Override
	public void deleteExam(Integer id) {
		dao.deleteExam(id);
	}

	@Override
	public List<Examquestions> findExamquestionsAll(Integer eid,String type) {
		return dao.findExamquestionsAll(eid,type);
	}

	@Override
	public Examquestions findSingleExamquestions(Integer id) {
		return dao.findSingleExamquestions(id);
	}

	@Override
	public void addOrUpdateExamquestions(Examquestions obj) {
		dao.addOrUpdateExamquestions(obj);
	}

	@Override
	public void deleteExamquestions(Integer id) {
		dao.deleteExamquestions(id);
	}

	@Override
	public void deleteExamquestionsByEid(Integer eid) {
		dao.deleteExamquestionsByEid(eid);
	}
}
