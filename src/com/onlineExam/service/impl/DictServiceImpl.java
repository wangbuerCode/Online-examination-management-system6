package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.DictDao;
import com.onlineExam.entity.City;
import com.onlineExam.entity.Province;
import com.onlineExam.service.DictService;
@Service
public class DictServiceImpl implements DictService {

	private DictDao dao;

	public DictDao getDao() {
		return dao;
	}

	public void setDao(DictDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Province> findProvinces() {
		return dao.findProvinces();
	}

	@Override
	public List<City> findCitiesByPid(Integer pid) {
		return dao.findCitiesByPid(pid);
	}

	@Override
	public Province findProvinceById(Integer pid) {
		return dao.findProvinceById(pid);
	}

	@Override
	public City findCityById(Integer cid) {
		return dao.findCityById(cid);
	}

	
}
