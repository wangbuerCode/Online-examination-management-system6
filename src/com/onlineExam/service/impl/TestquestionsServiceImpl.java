package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.TestquestionsDao;
import com.onlineExam.entity.Testquestions;
import com.onlineExam.service.TestquestionsService;
@Service
public class TestquestionsServiceImpl implements TestquestionsService {

	private TestquestionsDao dao;

	public TestquestionsDao getDao() {
		return dao;
	}

	public void setDao(TestquestionsDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Testquestions> findTestquestionsAll(String testtype) {
		return dao.findTestquestionsAll(testtype);
	}

	@Override
	public Testquestions findSingleTestquestions(Integer id) {
		return dao.findSingleTestquestions(id);
	}

	@Override
	public void addOrUpdateTestquestions(Testquestions obj) {
		dao.addOrUpdateTestquestions(obj);
	}

	@Override
	public void deleteTestquestions(Integer id) {
		dao.deleteTestquestions(id);
	}

	@Override
	public List<Testquestions> findTestquestionsAllBySidAndType(Integer sid,
			String qtype) {
		return dao.findTestquestionsAllBySidAndType(sid,qtype);
	}
}
