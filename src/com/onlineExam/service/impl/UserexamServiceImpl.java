package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.UserexamDao;
import com.onlineExam.entity.Userexam;
import com.onlineExam.service.UserexamService;
@Service
public class UserexamServiceImpl implements UserexamService {

	private UserexamDao dao;

	public UserexamDao getDao() {
		return dao;
	}

	public void setDao(UserexamDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Userexam> findUserexamAll() {
		return dao.findUserexamAll();
	}

	@Override
	public Userexam findSingleUserexam(Integer id) {
		return dao.findSingleUserexam(id);
	}

	@Override
	public void addOrUpdateUserexam(Userexam obj) {
		dao.addOrUpdateUserexam(obj);
	}

	@Override
	public void deleteUserexam(Integer id) {
		dao.deleteUserexam(id);
	}

	@Override
	public List<Userexam> findUserexamAllByUid(Integer uid) {
		return dao.findUserexamAllByUid(uid);
	}

	@Override
	public List<Object[]> findUserexamAllTj() {
		return dao.findUserexamAllTj();
	}
}
