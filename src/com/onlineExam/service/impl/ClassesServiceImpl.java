package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.ClassesDao;
import com.onlineExam.entity.Classes;
import com.onlineExam.service.ClassesService;
@Service
public class ClassesServiceImpl implements ClassesService {

	private ClassesDao dao;

	public ClassesDao getDao() {
		return dao;
	}

	public void setDao(ClassesDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Classes> findClassesAll(Integer collegeid) {
		return dao.findClassesAll(collegeid);
	}

	@Override
	public Classes findSingleClasses(Integer id) {
		return dao.findSingleClasses(id);
	}

	@Override
	public void addOrUpdateClasses(Classes obj) {
		dao.addOrUpdateClasses(obj);
	}

	@Override
	public void deleteClasses(Integer id) {
		dao.deleteClasses(id);
	}
}
