package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.CollegeDao;
import com.onlineExam.entity.College;
import com.onlineExam.service.CollegeService;
@Service
public class CollegeServiceImpl implements CollegeService {

	private CollegeDao dao;

	public CollegeDao getDao() {
		return dao;
	}

	public void setDao(CollegeDao dao) {
		this.dao = dao;
	}

	@Override
	public List<College> findCollegeAll() {
		return dao.findCollegeAll();
	}

	@Override
	public College findSingleCollege(Integer id) {
		return dao.findSingleCollege(id);
	}

	@Override
	public void addOrUpdateCollege(College obj) {
		dao.addOrUpdateCollege(obj);
	}

	@Override
	public void deleteCollege(Integer id) {
		dao.deleteCollege(id);
	}
}
