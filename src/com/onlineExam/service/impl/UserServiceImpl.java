package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.UserDao;
import com.onlineExam.entity.User;
import com.onlineExam.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	private UserDao dao;

	public UserDao getDao() {
		return dao;
	}

	public void setDao(UserDao dao) {
		this.dao = dao;
	}

	@Override
	public List<User> findUserAll(String role) {
		return dao.findUserAll(role);
	}

	@Override
	public boolean addOrUpdateUser(User userobj) {
		return dao.addOrUpdateUser(userobj);
	}

	@Override
	public User findSingleUser(Integer uid) {
		return dao.findSingleUser(uid);
	}

	@Override
	public void updatePwd(String uid, String newpwd) {
		dao.updatePwd(uid, newpwd);
	}

	@Override
	public User login(String username, String pwd, String role) {
		return dao.login(username,pwd,role);
	}

	@Override
	public void deleteUser(Integer userid) {
		dao.deleteUser(userid);
	}

}
