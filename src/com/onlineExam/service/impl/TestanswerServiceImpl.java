package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.TestanswerDao;
import com.onlineExam.entity.Testanswer;
import com.onlineExam.service.TestanswerService;
@Service
public class TestanswerServiceImpl implements TestanswerService {

	private TestanswerDao dao;

	public TestanswerDao getDao() {
		return dao;
	}

	public void setDao(TestanswerDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Testanswer> findTestanswerAll(Integer testquestionsid) {
		return dao.findTestanswerAll(testquestionsid);
	}

	@Override
	public Testanswer findSingleTestanswer(Integer id) {
		return dao.findSingleTestanswer(id);
	}

	@Override
	public void addOrUpdateTestanswer(Testanswer obj) {
		dao.addOrUpdateTestanswer(obj);
	}

	@Override
	public void deleteTestanswer(Integer id) {
		dao.deleteTestanswer(id);
	}

	@Override
	public void deleteTestanswerByTestquestionsid(int testquestionsid) {
		dao.deleteTestanswerByTestquestionsid(testquestionsid);
	}
}
