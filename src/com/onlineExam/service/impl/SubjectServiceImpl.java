package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.SubjectDao;
import com.onlineExam.entity.Subject;
import com.onlineExam.service.SubjectService;
@Service
public class SubjectServiceImpl implements SubjectService {

	private SubjectDao dao;

	public SubjectDao getDao() {
		return dao;
	}

	public void setDao(SubjectDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Subject> findSubjectAll() {
		return dao.findSubjectAll();
	}

	@Override
	public Subject findSingleSubject(Integer id) {
		return dao.findSingleSubject(id);
	}

	@Override
	public void addOrUpdateSubject(Subject obj) {
		dao.addOrUpdateSubject(obj);
	}

	@Override
	public void deleteSubject(Integer id) {
		dao.deleteSubject(id);
	}
}
