package com.onlineExam.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;

import com.onlineExam.dao.UserexamanswerDao;
import com.onlineExam.entity.Userexamanswer;
import com.onlineExam.service.UserexamanswerService;
@Service
public class UserexamanswerServiceImpl implements UserexamanswerService {

	private UserexamanswerDao dao;

	public UserexamanswerDao getDao() {
		return dao;
	}

	public void setDao(UserexamanswerDao dao) {
		this.dao = dao;
	}

	@Override
	public List<Userexamanswer> findUserexamanswerAll() {
		return dao.findUserexamanswerAll();
	}

	@Override
	public Userexamanswer findSingleUserexamanswer(Integer id) {
		return dao.findSingleUserexamanswer(id);
	}

	@Override
	public void addOrUpdateUserexamanswer(Userexamanswer obj) {
		dao.addOrUpdateUserexamanswer(obj);
	}

	@Override
	public void deleteUserexamanswer(Integer id) {
		dao.deleteUserexamanswer(id);
	}

	@Override
	public List<Userexamanswer> findUserexamanswerAllByTqid(Integer tqid) {
		return dao.findUserexamanswerAllByTqid(tqid);
	}

	@Override
	public Userexamanswer findUserexamanswerAllByInfo(Integer userexamid, Integer pid) {
		return dao.findUserexamanswerAllByInfo(userexamid,pid);
	}
}
