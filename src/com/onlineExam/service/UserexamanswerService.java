package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Userexamanswer;

public interface UserexamanswerService {

	public List<Userexamanswer> findUserexamanswerAll();

	public Userexamanswer findSingleUserexamanswer(Integer id);

	public void addOrUpdateUserexamanswer(Userexamanswer obj);

	public void deleteUserexamanswer(Integer id);

	public List<Userexamanswer> findUserexamanswerAllByTqid(Integer tqid);

	public Userexamanswer findUserexamanswerAllByInfo(Integer userexamid, Integer pid);
}
