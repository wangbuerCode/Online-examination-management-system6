package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Userexam;

public interface UserexamService {

	public List<Userexam> findUserexamAll();

	public Userexam findSingleUserexam(Integer id);

	public void addOrUpdateUserexam(Userexam obj);

	public void deleteUserexam(Integer id);

	public List<Userexam> findUserexamAllByUid(Integer uid);

	public List<Object[]> findUserexamAllTj();
}
