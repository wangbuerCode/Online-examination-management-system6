package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.College;

public interface CollegeService {

	public List<College> findCollegeAll();

	public College findSingleCollege(Integer id);

	public void addOrUpdateCollege(College obj);

	public void deleteCollege(Integer id);
}
