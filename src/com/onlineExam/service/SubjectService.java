package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Subject;

public interface SubjectService {

	public List<Subject> findSubjectAll();

	public Subject findSingleSubject(Integer id);

	public void addOrUpdateSubject(Subject obj);

	public void deleteSubject(Integer id);
}
