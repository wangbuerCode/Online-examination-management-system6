package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Classes;

public interface ClassesService {

	public List<Classes> findClassesAll(Integer collegeid);

	public Classes findSingleClasses(Integer id);

	public void addOrUpdateClasses(Classes obj);

	public void deleteClasses(Integer id);
}
