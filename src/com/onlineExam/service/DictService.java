package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.City;
import com.onlineExam.entity.Province;

public interface DictService {
	
	public List<Province> findProvinces();

	public List<City> findCitiesByPid(Integer pid);

	public Province findProvinceById(Integer pid);
	
	public City findCityById(Integer cid);
}
