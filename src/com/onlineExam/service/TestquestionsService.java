package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Testquestions;

public interface TestquestionsService {

	public List<Testquestions> findTestquestionsAll(String testtype);

	public Testquestions findSingleTestquestions(Integer id);

	public void addOrUpdateTestquestions(Testquestions obj);

	public void deleteTestquestions(Integer id);

	public List<Testquestions> findTestquestionsAllBySidAndType(Integer sid,
			String qtype);
}
