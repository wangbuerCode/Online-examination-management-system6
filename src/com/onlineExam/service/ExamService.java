package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Exam;
import com.onlineExam.entity.Examquestions;

public interface ExamService {

	public List<Exam> findExamAll();

	public Exam findSingleExam(Integer id);

	public void addOrUpdateExam(Exam obj);

	public void deleteExam(Integer id);
	
	public List<Examquestions> findExamquestionsAll(Integer eid, String type);

	public Examquestions findSingleExamquestions(Integer id);

	public void addOrUpdateExamquestions(Examquestions obj);

	public void deleteExamquestions(Integer id);
	
	public void deleteExamquestionsByEid(Integer eid);
}
