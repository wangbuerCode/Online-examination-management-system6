package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.Testanswer;

public interface TestanswerService {

	public List<Testanswer> findTestanswerAll(Integer testquestionsid);

	public Testanswer findSingleTestanswer(Integer id);

	public void addOrUpdateTestanswer(Testanswer obj);

	public void deleteTestanswer(Integer id);

	public void deleteTestanswerByTestquestionsid(int testquestionsid);
}
