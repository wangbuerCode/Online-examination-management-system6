package com.onlineExam.service;

import java.util.List;

import com.onlineExam.entity.User;

public interface UserService {

    public List<User> findUserAll(String role);

    public boolean addOrUpdateUser(User userobj);

    public User findSingleUser(Integer uid);

    public void updatePwd(String uid, String newpwd);

	public User login(String username, String pwd, String role);

	public void deleteUser(Integer userid);

}
